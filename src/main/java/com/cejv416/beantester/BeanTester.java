package com.cejv416.beantester;

// Required for Arrays.sort
import com.cejv416.bean.Book;
import java.util.Arrays;

public class BeanTester {

    /**
     * Here is where I am testing my comparisons
     *
     */
    public void perform() {
        // Lets create four books
        Book b0 = new Book("200", "Xenon", "Hamilton", "Harcourt", 99);
        Book b1 = new Book("500", "Boron", "Bradbury", "Prentice", 108);
        Book b2 = new Book("300", "Radon", "Heinlein", "Thompson", 98);
        Book b3 = new Book("404", "Argon", "Campbell", "Hachette", 102);

        // Using equals to compare two books
        System.out.println("Value return by equals");
        System.out.println("Is b0 equal to b1? " + b0.equals(b1));

        // Using hashCode to compare two books
        System.out.println("Hash code values");
        System.out.println("hashCode of b0: " + b0.hashCode());
        System.out.println("hashCode of b1: " + b1.hashCode());

        // Using Comparable to compare two books
        System.out.println("Value returned by Comparable");
        System.out.println(b0.getTitle() + " compared to " + b1.getTitle() + ": "
                + b0.compareTo(b1));
        System.out.println();

        // Create an array we can sort
//        Book[] myBooks = new Book[4];
//        myBooks[0] = b0;
//        myBooks[1] = b1;
//        myBooks[2] = b2;
//        myBooks[3] = b3;
//        System.out.println("Unsorted");
//        displayBooks(myBooks);
//
//        System.out.println("Sorted with Comparable Interface on Title");
//        Arrays.sort(myBooks); // uses the Comparable compareTo in the bean
//        displayBooks(myBooks);
//
//        Book bb0 = doDeepCopy(b0);
//        System.out.println(" b0: " + b0);
//        System.out.println("bb0: " + bb0);

//        System.out.println("Sorted with Comparable lambda expression on Publishers");
//        Arrays.sort(myBooks, (s1, s2) -> {
//            return s1.getPublisher().compareTo(s2.getPublisher());
//        }); // uses the Comparator lambda
//        displayBooks(myBooks);
//
//        System.out.println("Sorted with Comparable lambda functions based on ISBN");
//        Arrays.sort(myBooks, comparing(Book::getIsbn)); // Comparable function
//        displayBooks(myBooks);
    }

    /**
     * Print the contents of each Book object in the array
     *
     * @param theBooks
     */
    private void displayBooks(Book[] theBooks) {
        for (Book b : theBooks) {
            System.out.print(b.getIsbn() + "\t");
            System.out.print(b.getTitle() + "\t");
            System.out.print(b.getAuthor() + "\t");
            System.out.print(b.getPublisher() + "\t");
            System.out.println(b.getPages());
        }
        System.out.println();
    }

    private Book doDeepCopy(Book original) {
        Book copy = new Book(original.getIsbn(), original.getTitle(), original.getAuthor(), original.getPublisher(), original.getPages());
        System.out.println("original.equals(copy) = " + original.equals(copy));

        return copy;
    }

/**
 * Where it all begins
 *
 * @param args
 */
public static void main(String[] args) {
        new BeanTester().perform();
    }
}
