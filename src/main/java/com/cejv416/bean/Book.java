package com.cejv416.bean;

import java.util.Objects;

/**
 * This is a simple JavaBean that overrides equals and hashCode that are
 * inherited from Object. It implements the Comparable interface to determine if
 * one object is greater than, equal to, or less than the other. This is an
 * arbitrary decision that you make by implementing a compareTo method.
 *
 * @author Ken Fogel
 */
public class Book implements Comparable<Book> {

    private String isbn;
    private String title;
    private String author;
    private String publisher;
    private int pages;

    /**
     * Default constructor
     */
    public Book() {
        this.isbn = "";
        this.title = "";
        this.author = "";
        this.publisher = "";
        this.pages = 0;
    }

    /**
     * Non-default constructor
     *
     * @param isbn
     * @param title
     * @param author
     * @param publisher
     * @param pages
     */
    public Book(final String isbn, final String title, final String author, final String publisher, final int pages) {
        this.isbn = isbn;
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.pages = pages;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(final String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(final String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(final String publisher) {
        this.publisher = publisher;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(final int pages) {
        this.pages = pages;
    }

    @Override
    public String toString() {
        return "Book{" + "isbn=" + isbn + ", title=" + title + ", author=" + author + ", publisher=" + publisher + ", pages=" + pages + '}';
    }

    /**
     * This method returns an integer constructed from the fields of the object.
     * This is not a unique value. If two objects generate different hash codes
     * then they are definitively not the same. If two objects generate the same
     * hash codes then they may still not be the same. When hash codes are the
     * same you must test with equals. The equals method is always slower than
     * the hashCode method. This is why we test with hashCode first.
     *
     * @return hashCode value
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.isbn);
        hash = 61 * hash + Objects.hashCode(this.title);
        hash = 61 * hash + Objects.hashCode(this.author);
        hash = 61 * hash + Objects.hashCode(this.publisher);
        hash = 61 * hash + this.pages;
        return hash;
    }

    /**
     * This method, after determining that the objects can be tested for
     * equality, examines every field using either operators for primitives or
     * equals methods for objects. You could call this a deep comparison.
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        // Both references pointing to the same object
        if (this == obj) {
            return true;
        }
        // Object to test for equality is null
        if (obj == null) {
            return false;
        }
        // Objects are not the same class type
        if (getClass() != obj.getClass()) {
            return false;
        }
        // Cast obj to type Book
        final Book other = (Book) obj;

        // Field by field test
        // test of primituves can use operators != or ==
        if (this.pages != other.pages) {
            return false;
        }

        // The remaining fields are objects of type String so we use their 
        // equals methods
        if (!Objects.equals(this.isbn, other.isbn)) {
            return false;
        }
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        if (!Objects.equals(this.author, other.author)) {
            return false;
        }
        return Objects.equals(this.publisher, other.publisher);
    }

    /**
     * Implementation of Comparable interface. Strings implement Comparable so
     * we call the compareTo in them
     *
     * @param book
     * @return negative value <, 0 ==, or positive > value
     */
    @Override
    public int compareTo(Book book) {
        return title.compareTo(book.title);
    }
}
